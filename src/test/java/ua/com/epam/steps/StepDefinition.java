package ua.com.epam.steps;


import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ua.com.epam.asserts.LoginAsserter;
import ua.com.epam.asserts.MessageAsserter;
import ua.com.epam.factory.DriverProvider;
import ua.com.epam.ui.actions.LoginActions;
import ua.com.epam.ui.actions.MessageActions;
import ua.com.epam.ui.actions.NavigationActions;

import java.io.IOException;


public class StepDefinition {
    protected LoginActions loginActions = new LoginActions();
    protected LoginAsserter loginAsserter = new LoginAsserter();
    protected MessageActions messageActions = new MessageActions();
    protected MessageAsserter messageAsserter = new MessageAsserter();
    protected NavigationActions navigationActions = new NavigationActions();

    @After
    public void tearDown() throws IOException {
        DriverProvider.quitDriver();
    }

    @Given("Log in to gmail with {string} and {string}")
    public void logInToGmailWithLoginAndPassword(String login, String password) {
        navigationActions.navigateToLoginPage();
        loginActions.logInToAccount(login, password);
    }

    @Then("Verify successful log in to account {string}")
    public void verifySuccessfulLogInIntoAccount(String login) {
        loginAsserter.assertSuccessfulLogIn(login);
    }

    @When("Create new draft message with {string}, {string}, {string}")
    public void createNewDraftMessageWith(String recipient, String subject, String message) {
        messageActions.fillInNewMessage(recipient, subject, message);
        messageActions.closeMessageForm();
    }

    @Then("Verify message with {string} added to drafts")
    public void verifyMessageWithAddedToDrafts(String subject) {
        messageActions.openDrafts();
        messageAsserter.assertMessageAddedToDrafts(subject);
    }

    @Given("Open created draft message with {string}")
    public void openCreatedDraftMessageWith(String subject) {
        messageActions.openMessage(subject);
    }

    @When("User sends draft message")
    public void userSendsDraftMessageWith() {
        messageActions.sendMessage();
    }

    @Then("Verify message send")
    public void verifyMessageSend() {
        messageAsserter.assertMessageSent();
    }
}
