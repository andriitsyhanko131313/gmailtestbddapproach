package ua.com.epam;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(features = "src/test/resources/ua.com.epam.gmail/",
        glue = "ua.com.epam.steps")
public class TestRunner extends AbstractTestNGCucumberTests {
}
