Feature: As a user I want to create draft message and send created draft message from drafts

  Scenario Outline: Log in to gmail, create draft message, send created draft message from drafts
    Given Log in to gmail with "<login>" and "<password>"
    Then Verify successful log in to account "<login>"
    When Create new draft message with "<recipient>", "<subject>", "<message>"
    Then Verify message with "<subject>" added to drafts
    Given Open created draft message with "<subject>"
    When User sends draft message
    Then Verify message send
    Examples:
      | login                 | password        | recipient             | subject  | message  |
      | testsel77@gmail.com   | Qwerty123qwerty | tpageobject@gmail.com | subject1 | message1 |
      | tpageobject@gmail.com | Pageobject1234  | seltest798@gmail.com  | subject2 | message2 |
      | testsel874@gmail.com  | Asdfgh1234      | stest2631@gmail.com   | subject3 | message3 |
      | seltest798@gmail.com  | Zxcvbnm1234Z    | testsel77@gmail.com   | subject4 | message4 |
      | stest2631@gmail.com   | QAZxcde132      | testsel874@gmail.com  | subject5 | message5 |

