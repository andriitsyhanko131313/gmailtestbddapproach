package ua.com.epam.ui.actions;

import ua.com.epam.ui.pages.DraftsPage;
import ua.com.epam.ui.pages.HomePage;
import ua.com.epam.ui.pages.NewMessagePage;


public class MessageActions {
    private final HomePage homePage;
    private final DraftsPage draftsPage;
    private final NewMessagePage newMessagePage;

    public MessageActions() {
        homePage = new HomePage();
        draftsPage = new DraftsPage();
        newMessagePage = new NewMessagePage();
    }

    public void fillInNewMessage(String recipient, String subject, String message) {
        homePage.clickComposeButton();
        newMessagePage.setRecipientsField(recipient);
        newMessagePage.setSubjectField(subject);
        newMessagePage.setMessageField(message);
    }

    public void closeMessageForm() {
        newMessagePage.clickCloseButton();
    }

    public void openMessage(String subject) {
        draftsPage.getMessageBySubject(subject).saveClick();
    }

    public void openDrafts() {
        homePage.setSearchEmailField("in:draft");
    }

    public void sendMessage() {
        newMessagePage.clickSendMessageButton();
    }
}
