package ua.com.epam.ui.pages;


import org.openqa.selenium.support.PageFactory;
import ua.com.epam.decorator.CustomFieldDecorator;
import ua.com.epam.factory.DriverProvider;

public abstract class AbstractPage {
    protected AbstractPage() {
        PageFactory.initElements(new CustomFieldDecorator(DriverProvider.getDriver()), this);
    }
}
