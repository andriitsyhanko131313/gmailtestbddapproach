package ua.com.epam.ui.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.decorator.elements.Button;
import ua.com.epam.decorator.elements.Input;
import ua.com.epam.decorator.elements.Label;
import ua.com.epam.utils.WaitUtils;

import static ua.com.epam.constants.Constants.*;


public class HomePage extends AbstractPage {
    @FindBy(xpath = "//a[contains(@aria-label,'Google Account:')]")
    private Label accountInformation;

    @FindBy(css = "div.T-I.T-I-KE.L3")
    private Button composeButton;

    @FindBy(xpath = "//span[@class='aT']/span[@class='bAq']")
    private Label informMessage;

    @FindBy(xpath = "//input[@placeholder='Search mail']")
    private Input searchEmail;

    public void clickComposeButton() {
        composeButton.saveClick();
    }

    public String getAccountInformation() {
        WaitUtils.waitForPageLoadComplete();
        return accountInformation.getAttribute(ATTRIBUTE_ARIA_LABEL);
    }

    public void setSearchEmailField(CharSequence charSequence) {
        searchEmail.waitForFieldReadyToInput();
        searchEmail.clearAndSendKeys(charSequence, Keys.ENTER);
    }

    public Label getInformationMessage() {
        return informMessage;
    }
}

