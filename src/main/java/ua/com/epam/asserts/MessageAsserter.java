package ua.com.epam.asserts;

import org.testng.Assert;
import ua.com.epam.ui.pages.DraftsPage;
import ua.com.epam.ui.pages.HomePage;
import ua.com.epam.utils.WaitUtils;

import static ua.com.epam.constants.Constants.SUCCESSFULLY_SENT_MESSAGE;

public class MessageAsserter {
    private final HomePage homePage;
    private final DraftsPage draftsPage;

    public MessageAsserter() {
        homePage = new HomePage();
        draftsPage = new DraftsPage();
    }

    public void assertMessageAddedToDrafts(String subject) {
        WaitUtils.waitForVisibility(draftsPage.getMessageBySubject(subject));
        boolean actual = draftsPage.getMessageBySubject(subject).isDisplayed();
        Assert.assertTrue(actual, String.format("Expected message [%s], but found [%s]", actual, false));
    }

    public void assertMessageSent() {
        String actualText = homePage.getInformationMessage().waitTextToBe(SUCCESSFULLY_SENT_MESSAGE).getText();
        Assert.assertEquals(actualText, SUCCESSFULLY_SENT_MESSAGE,
                String.format("Expected message [%s], but found [%s]", SUCCESSFULLY_SENT_MESSAGE, actualText));
    }

}
