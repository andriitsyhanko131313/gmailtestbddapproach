package ua.com.epam.asserts;

import org.testng.Assert;
import ua.com.epam.ui.pages.HomePage;

public class LoginAsserter {
    private final HomePage homePage;

    public LoginAsserter() {
        homePage = new HomePage();
    }

    public void assertSuccessfulLogIn(String userLogin) {
        String actual = homePage.getAccountInformation();
        Assert.assertTrue(actual.contains(userLogin),
                String.format("Expected user login  [%s], but found [%s]", userLogin, actual));
    }

}
