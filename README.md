#GmailTestBDDApproach  project

created by [Andrii Tsyhanko](https://gitlab.com/andriitsyhanko131313)

#Project Documentation

* [Selenium](https://github.com/SeleniumHQ/selenium)
* [TestNG](https://testng.org/doc/)
* [Log4j2](https://github.com/apache/logging-log4j2)
* [Allure Report](https://docs.qameta.io/allure/)
* [Cucumber](https://github.com/cucumber)

###Project Requirements
* [Java 1.8](https://www.oracle.com/ru/java/technologies/javase/javase-jdk8-downloads.html)
* [Maven Apache](https://maven.apache.org/)

##Run Project
`mvn clean test`


## Additional Information
Realization Page Factory Pattern `src/main/java/ua/com/epam/ui/pages`
Realization Custom Wrapper for WebElement `src/main/java/ua/com/epam/decorator`
Realization Actions layer `src/main/java/ua/com/epam/ui/actions`
`

Project main configs : `src/main/resources/config.properties`
